# import viewsets 
from rest_framework import viewsets 
  
# import local data 
from .serializers import TimeSerializer 
from .models import TimeModel 
  
# create a viewset 
class TimeViewSet(viewsets.ModelViewSet): 
    # define queryset 
    queryset = TimeModel.objects.all() 
      
    # specify serializer to be used 
    serializer_class = TimeSerializer 
